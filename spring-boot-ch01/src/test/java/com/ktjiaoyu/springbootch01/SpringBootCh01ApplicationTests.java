package com.ktjiaoyu.springbootch01;

import com.ktjiaoyu.springbootch01.service.DemoService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import javax.swing.*;
import java.sql.PreparedStatement;

@RunWith(SpringRunner.class)

@SpringBootTest
public class SpringBootCh01ApplicationTests {

    @Resource
    private DemoService demoService;

    @Test
    public void contextLoads() {
        String msg=demoService.sayHello();
        System.out.println("msg"+msg);
    }

}

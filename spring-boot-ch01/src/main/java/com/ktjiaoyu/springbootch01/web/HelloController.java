package com.ktjiaoyu.springbootch01.web;

import com.ktjiaoyu.springbootch01.comm.KtjiaoyuInfo;
import com.ktjiaoyu.springbootch01.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class HelloController {
//    @Autowired
//    private DemoService demoService;
//    @RequestMapping("/hello")
//    public String hello(){
//        String msg=demoService.sayHello();
//        return msg;
//    }

//    @Value("${com.tjz.name}")
//    private String name;
//    @Value("${com.tjz.email}")
//    private String email;
//    @Autowired
//    private DemoService demoService;
//    @RequestMapping("/hello")
//    public String hello(){
//        String msg=demoService.sayHello();
//        msg+=" "+name+" "+email;
//        return msg;
//    }

    @Resource
    private KtjiaoyuInfo ktjiaoyuInfo;
    @Autowired
    private DemoService demoService;
    @RequestMapping("/hello")
    public String hello(){
        String msg=demoService.sayHello();
        msg+=" "+ktjiaoyuInfo.getName()+" "+ktjiaoyuInfo.getEmail();
        return msg;
    }

}

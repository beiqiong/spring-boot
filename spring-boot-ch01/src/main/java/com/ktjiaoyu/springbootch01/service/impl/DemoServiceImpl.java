package com.ktjiaoyu.springbootch01.service.impl;

import com.ktjiaoyu.springbootch01.service.DemoService;
import org.springframework.stereotype.Service;

@Service("demoService")
public class DemoServiceImpl implements DemoService {
    @Override
    public String sayHello() {
        return "Hello Word";
    }
}

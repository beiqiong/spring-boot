package com.ktjiaoyu.springbootch01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootCh01Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootCh01Application.class, args);
    }

}

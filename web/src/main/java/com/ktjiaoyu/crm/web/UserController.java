package com.ktjiaoyu.crm.web;

import com.ktjiaoyu.crm.pojo.User;
import com.ktjiaoyu.crm.service.UserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class UserController {
    @Resource
    private UserService userService;

    @RequestMapping(value = "getUser",method = RequestMethod.GET)
    public User getUser(Long usrId){
        User user=userService.getUser(usrId);
        return user;
    }

}

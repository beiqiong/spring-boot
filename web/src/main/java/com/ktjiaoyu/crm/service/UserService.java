package com.ktjiaoyu.crm.service;

import com.ktjiaoyu.crm.pojo.User;

import java.util.List;

public interface UserService {
    public User login(String usrName,String usrPassword);
    public int addUser(User user);
    public int deleteUser(Long usrId);
    public int updateUser(User user);
    public User getUser(Long usrId);
    public List<User> findAllUsers();
}

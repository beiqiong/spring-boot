package com.ktjiaoyu.crm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ktjiaoyu.crm.entity.User;


public interface UserMapper extends BaseMapper<User> {

}

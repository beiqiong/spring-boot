package com.ktjiaoyu.crm.mapper;

import com.ktjiaoyu.crm.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserMapperTest {
    @Resource
    private UserMapper userMapper;

    @Test
    public void testSelectById(){
        User user=userMapper.selectById(2L);
        System.out.println("usrName:"+user.getUsrName());
    }
}
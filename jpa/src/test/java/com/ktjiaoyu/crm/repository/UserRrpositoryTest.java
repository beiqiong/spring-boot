package com.ktjiaoyu.crm.repository;

import com.ktjiaoyu.crm.entity.User;
import com.ktjiaoyu.crm.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRrpositoryTest {

    @Resource
    private UserRrpository userRrpository;

    @Resource
    private UserService userService;


    @Test
    public void testInsert(){//测试新增
        User user = new User("JSP","123456",8L,1);
        userRrpository.save(user);
    }

    @Test
    public void testGet(){//测试主键

        User user=userRrpository.findById(2L).get();
        System.out.println("usrName:"+user.getUsrName());
    }

    @Test
    public void testFindByUsrNameLike(){//测试按用户模糊查询
        List<User>list=userRrpository.findByUsrNameLike("%s%");//查询参数必须带%号
        if (list!=null){
            for (User user:list){
                System.out.println("usrName:"+user.getUsrName());
            }
        }
    }

    @Test
    public void testFindPageByUsrRoleId(){//测试按角色查询用户并分页
        int page=0,size=2;


        Sort sort = Sort.by(Sort.Direction.DESC,"usrId");

        Pageable pageable= PageRequest.of(page,size,sort);
        Page<User> userPage=userRrpository.findPageByUsrRoleId(8L,pageable);

        System.out.println("总记录数："+userPage.getTotalElements());
        System.out.println("总页数："+userPage.getTotalPages());
        System.out.println("当前页数："+(userPage.getNumber()+1));
        System.out.println("每页记录数："+userPage.getSize());
        System.out.println("当页记录数："+userPage.getNumberOfElements());
        for (User user:userPage.getContent()){//获得查询记录
            System.out.println("usrId:"+user.getUsrId());
        }
    }
    @Test
    public void findPageByMap(){
        int page =0,size=2;//分页要素，页数从0开始
        //控制分页数据的排序，可以选择升序和降序
        Sort sort = Sort.by(Sort.Direction.DESC,"userId");
        //控制分页的辅助类，可以设置页码，每页的数据条数，排序
        Pageable pageable = PageRequest.of(page,size,sort);
        Map param = new HashMap();
        param.put("roleId",8L);
        Page<User> userPage = userService.findPageByMap(param,pageable);

        System.out.println("总记录数："+userPage.getTotalElements());
        System.out.println("总页数："+userPage.getTotalPages());
        System.out.println("当前页数："+(userPage.getNumber())+1);
        System.out.println("每页记录数："+userPage.getSize());
        System.out.println("当前记录：------>");

        for (User user : userPage.getContent()){
            System.out.println("usrId:"+user.getUsrId());
        }
    }
}
package com.ktjiaoyu.crm.service.impl;

;
import com.ktjiaoyu.crm.entity.User;
import com.ktjiaoyu.crm.repository.UserRrpository;
import com.ktjiaoyu.crm.service.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserRrpository userRrpository;

    @Override
    public Page<User> findPageByMap(Map param, Pageable pageable) {
        return userRrpository.findAll(new Specification<User>() {
            @Override
            public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> predicates = new ArrayList<>();
                if(param.get("usrName")!=null){
                    predicates.add(cb.like(root.get("usrName"),"%"+param.get("usrName")+"%"));
                }
                if(param.get("roleId")!=null){
                    predicates.add(cb.equal(root.get("usrRoleId"),param.get("roleId")));
                }
                return cb.and(predicates.toArray(new Predicate[predicates.size()]));
            }
        },pageable);
    }
}

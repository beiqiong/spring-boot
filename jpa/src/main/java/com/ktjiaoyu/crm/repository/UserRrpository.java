package com.ktjiaoyu.crm.repository;

import com.ktjiaoyu.crm.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface UserRrpository extends JpaRepository<User,Long> {

    public List<User> findByUsrNameLike(String usrName);

    @Query("select u from User u where u.usrRoleId=?1")
    public Page<User>findPageByUsrRoleId(Long roleId, Pageable pageable);

    Page<User> findAll(Specification<User> userSpecification, Pageable pageable);
}

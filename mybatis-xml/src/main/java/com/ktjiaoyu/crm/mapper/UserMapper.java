package com.ktjiaoyu.crm.mapper;

import com.ktjiaoyu.crm.entity.User;

import java.util.List;

public interface UserMapper {
    public void insert(User user);
    public void delete(Long id);
    public void update(User user);
    public User get(Long id);
    public List<User> finAll();
}

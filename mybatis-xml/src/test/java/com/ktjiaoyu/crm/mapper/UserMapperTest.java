package com.ktjiaoyu.crm.mapper;

import com.ktjiaoyu.crm.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserMapperTest {


    @Resource
    private UserMapper userMapper;

    @Test
    public void testInsert() throws Exception{
        userMapper.insert(new User("ktjiaoyu","123456",8L,1));
    }

    @Test
    public void testGet(){
        User user=userMapper.get(2L);
        System.out.println("usrName:"+user.getUsrName());
    }
}